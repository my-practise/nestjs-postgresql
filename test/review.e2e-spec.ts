import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { ReviewDto } from '../src/review/dto/review.dto';

const testDto: ReviewDto = {
  name: 'Lana',
  title: 'Test tittle',
  description: 'This is Test  DTO',
  rating: 5,
  productId: 1,
};

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let creatId: number;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/review/create (POST)', async (done) => {
    return request(app.getHttpServer())
      .post('/review/create')
      .send(testDto)
      .expect(201)
      .then(({ body }: request.Response) => {
        creatId = body.id;
        expect(creatId).toBeDefined();
        done();
      });
  });

  it('/review/:id (DELETE)', () => {
    return request(app.getHttpServer())
      .delete('/review/' + creatId)
      .expect(200);
  });
});
