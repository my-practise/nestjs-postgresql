import { Column, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

export enum TopLevelCategory {
  Courses,
  Services,
  Books,
  Products,
}
class Advantages {
  @Column()
  title: string;
  @Column()
  description: string;
}
class HeadHanter {
  @Column()
  count: number;
  @Column()
  juniorSalary: number;
  @Column()
  middleSalary: number;
  @Column()
  seniorSalary: number;
  @UpdateDateColumn()
  updateAt:Date

}

@Entity('top_page')
export class TopPageEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    type: 'enum',
    enum: TopLevelCategory,
    default: TopLevelCategory.Courses,
  })
  firstLevelCategory: TopLevelCategory;
  @Column()
  secondCategory: string;
  @Column()
  title: string;
  @Column()
  category: string;

  @Column({
    type: 'json',
    default: {},
  })
  hh: HeadHanter;
  @Column({
    type: 'jsonb',
  })
  advantages: Advantages[];
  @Column()
  seoText: string;
  @Column()
  tagsTittle: string;
  @Column({ type: 'simple-array' })
  tags: string[];
}
