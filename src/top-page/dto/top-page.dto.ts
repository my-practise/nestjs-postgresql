import { TopLevelCategory } from '../top-page.entity';
import { IsArray, IsEnum, IsJSON, IsNumber, IsString } from 'class-validator';

export class TopPageDto {
  @IsEnum(TopLevelCategory)
  firstLevelCategory: TopLevelCategory;
  @IsString()
  secondCategory: string;
  @IsString()
  title: string;
  @IsString()
  category: string;
  @IsJSON()

  hh: {

    count: number;

    juniorSalary: number;

    middleSalary: number;

    seniorSalary: number;
    updateAt:Date;
  };
  @IsArray()
  @IsJSON()
  @IsString({ each: true })
  advantages: {
    title: string;
    description: string;
  }[];

  @IsString()
  seoText: string;
  @IsString()
  tagsTittle: string;
  @IsArray()
  @IsString({ each: true })
  tags: string[];
}
