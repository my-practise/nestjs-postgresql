import { TopLevelCategory } from '../top-page.entity';


export class FindTopPageDto {

  category: TopLevelCategory;
}
