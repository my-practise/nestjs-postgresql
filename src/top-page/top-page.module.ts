import { Module } from '@nestjs/common';
import { TopPageController } from './top-page.controller';
import { TopPageEntity } from './top-page.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TopPageService } from './top-page.service';

@Module({
  imports: [TypeOrmModule.forFeature([TopPageEntity])],
  controllers: [TopPageController],
  providers: [TopPageService],
  exports:[TopPageService]
})
export class TopPageModule {}
