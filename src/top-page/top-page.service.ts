import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository,LessThan } from 'typeorm';
import { TopPageEntity } from './top-page.entity';
import { TopPageDto } from './dto/top-page.dto';
import { FindTopPageDto } from './dto/find-top-page.dto';
import { addDays } from "date-fns";

@Injectable()
export class TopPageService {
  constructor(
    @InjectRepository(TopPageEntity)
    private readonly topPageEntity: Repository<TopPageEntity>,
  ) {}
  async create(dto: TopPageDto) {
    return await this.topPageEntity.save(dto);
  }
  async getPage(id: number): Promise<TopPageEntity> {
    return await this.topPageEntity.findOneBy({ id: id });
  }

  async delete(id: number) {
    return await this.topPageEntity.delete(id);
  }

  async update(id: number, dto: TopPageDto) {
    return await this.topPageEntity.update({ id: id }, { ...dto });

  }
  async findAll() {
    return this.topPageEntity.find();
  }

  async findByCategory(dto: FindTopPageDto): Promise<TopPageEntity[]> {
    return this.topPageEntity.find({
      select: {
        secondCategory: true,
        title: true,
      },
      where: {
        firstLevelCategory: dto.category,
      },
    });
  }
  async findForHh(date:Date): Promise<TopPageEntity[]> {
    return this.topPageEntity.find({

      where: {
        firstLevelCategory: 0,
        hh:LessThan(addDays(date,-1))
      },
    });
  }
}
