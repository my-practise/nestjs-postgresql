import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Patch,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { TopPageDto } from './dto/top-page.dto';
import { FindTopPageDto } from './dto/find-top-page.dto';
import { TopPageService } from './top-page.service';
import { JWTAuthGuard } from '../auth/guards/auth.guard';

@Controller('top-page')
export class TopPageController {
  constructor(private readonly topPageService: TopPageService) {}
  @UseGuards(JWTAuthGuard)
  @Post('create')
  async create(@Body() dto: TopPageDto) {
    return this.topPageService.create(dto);
  }
  @UseGuards(JWTAuthGuard)
  @Get(':id')
  async get(@Param('id') id: number) {
    const topPage = await this.topPageService.getPage(id);

    if (!topPage) {
      throw new NotFoundException('Netu takovo');
    }
    return topPage;
  }
  @UseGuards(JWTAuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: number) {
    const deletePage = await this.topPageService.delete(id);
    if (!deletePage) {
      throw new NotFoundException('Netu takovo');
    }
    return deletePage;
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() dto: TopPageDto) {
    const updatePage = await this.topPageService.update(id, dto);
    if (!updatePage) {
      throw new NotFoundException('Netu takovo');
    }
    return updatePage;
  }
  @UsePipes(new ValidationPipe())
  @HttpCode(200)
  @Post('find')
  async find(@Body() dto: FindTopPageDto) {
    return await this.topPageService.findByCategory(dto);
  }
}
