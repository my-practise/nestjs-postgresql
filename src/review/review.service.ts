import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReviewEntity } from './review.entity';
import { DeepPartial, DeleteResult, ObjectID, Repository } from 'typeorm';
import { ReviewDto } from './dto/review.dto';
import { ProductEntity } from '../product/product.entity';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString, Max, Min } from 'class-validator';

@Injectable()
export class ReviewService {
  constructor(
    @InjectRepository(ReviewEntity)
    private readonly reviewEntity: Repository<ReviewEntity>,
  ) {}

  async create(dto: ReviewDto, product: ProductEntity) {
    const newReview = await this.reviewEntity.save({
      name: dto.name,

      title: dto.title,

      description: dto.description,

      rating: dto.rating,
      productsId: dto.productId,
    });
    product.review = [...product.review, newReview];

    await product.save();

    return newReview;
  }

  async delete(id: number): Promise<DeleteResult> {
    return this.reviewEntity.delete(id);
  }

  async findByProductId(productId: number) {
    return this.reviewEntity.findOne({
      where: {
        productsId: productId,
      },
    });
  }

  async deleteByProductId(productId: number): Promise<DeleteResult> {
    return this.reviewEntity.delete({ productsId: productId });
  }
}
