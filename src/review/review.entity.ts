import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  ObjectID,
  OneToMany,
  PrimaryGeneratedColumn,
  RelationId,
} from 'typeorm';
import { ForeignKeyMetadata } from 'typeorm/metadata/ForeignKeyMetadata';
import { ProductEntity } from '../product/product.entity';

@Entity('review')
export class ReviewEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  title: string;
  @Column()
  description: string;
  @Column()
  rating: number;
  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createAt: Date;
  @Column()
  productsId: number;

  @ManyToOne(() => ProductEntity, (products) => products.review, {
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  products: ProductEntity;
}
