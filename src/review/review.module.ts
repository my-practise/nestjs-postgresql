import { Module } from '@nestjs/common';
import { ReviewController } from './review.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReviewEntity } from './review.entity';
import { ReviewService } from './review.service';
import { ProductService } from '../product/product.service';
import { ProductEntity } from '../product/product.entity';
import { TelegramModule } from "../telegram/telegram.module";

@Module({
  imports: [TypeOrmModule.forFeature([ReviewEntity, ProductEntity]),TelegramModule],
  controllers: [ReviewController],
  providers: [ReviewService, ProductService],
})
export class ReviewModule {}
