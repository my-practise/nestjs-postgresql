import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ReviewDto } from './dto/review.dto';
import { ReviewService } from './review.service';
import { REVIEW_NOT_FOUND } from './review.constants';
import { JWTAuthGuard } from '../auth/guards/auth.guard';
import { UserEmail } from '../decarators/email.decarators';
import { ProductService } from '../product/product.service';
import { TelegramService } from "../telegram/telegram.service";

@Controller('review')
export class ReviewController {
  constructor(
    private readonly reviewService: ReviewService,
    private readonly productService: ProductService,
    private readonly telegramService: TelegramService
  ) {}
  @UseGuards(JWTAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post('create')
  async create(@Body() dto: ReviewDto) {
    const product = await this.productService.findById(dto.productId);
    return this.reviewService.create(dto, product);
  }

  @UsePipes(new ValidationPipe())
  @Post('notify')
  async notify(@Body() dto: ReviewDto) {
    const message = `Имя: ${dto.name}\n`
      + `Заголовок: ${dto.title}\n`
      + `Описание: ${dto.description}\n`
      + `Рейтинг: ${dto.rating}\n`
      + `ID Продукта: ${dto.productId}`;
    return this.telegramService.sendMessage(message);
  }
  @UseGuards(JWTAuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: number) {
    const deleteDoc = await this.reviewService.delete(id);

    if (!deleteDoc) {
      throw new HttpException(REVIEW_NOT_FOUND, HttpStatus.NOT_FOUND);
    }
  }
  @UseGuards(JWTAuthGuard)
  @Get('byProduct/:productId')
  async getByProduct(
    @Param('productId') productId: number,
    @UserEmail() email: string,
  ) {
    console.log(email);
    return this.reviewService.findByProductId(productId);
  }

  @Delete('deleteProduct/:productId')
  async deleteByProductId(@Param('productId') productId: number) {
    return this.reviewService.deleteByProductId(productId);
  }
}
