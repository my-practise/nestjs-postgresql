import { TopLevelCategory } from "../top-page/top-page.entity";

type routeMapType = Record<TopLevelCategory, string>

export const CATEGORY_URL:routeMapType={
  0:'/courses',
  1:'/services',
  2:'Books',
   3: 'Products'
}