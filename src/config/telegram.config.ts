import { TelegramOptions } from "../telegram/telegram.options";
import { ConfigService } from "@nestjs/config";

export const getTelegramConfig = (configService:ConfigService):TelegramOptions =>{
  const token = configService.get('TELEGRAM_TOKEN');
  const chatId=configService.get('CHAT_ID') ;
  if(!token){
    throw new Error("Netu Tokena")
  }
  if (!chatId){
    throw new Error("NETU ChatId")
  }

  return {
     token,
    chatId
  }
}