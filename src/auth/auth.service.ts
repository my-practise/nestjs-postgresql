import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AuthEntity } from './auth.entity';
import { AuthDto } from './dto/auth.dto';
import { compare, genSalt, hash } from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(AuthEntity)
    private readonly authEntity: Repository<AuthEntity>,
    private readonly jwtService: JwtService,
  ) {}

  async register(dto: AuthDto) {
    const salt = await genSalt(10);
    const newUser = new AuthEntity();
    newUser.email = dto.login;
    newUser.password = await hash(dto.password, salt);

    return this.authEntity.save(newUser);
  }
  async findUser(email: string) {
    return this.authEntity.findOneBy({ email });
  }

  async validateUser(
    email: string,
    password: string,
  ): Promise<Pick<AuthEntity, 'email'>> {
    const user = await this.findUser(email);
    if (!user) {
      throw new UnauthorizedException('Не авторизован');
    }
    const isCorrectPass = await compare(password, user.password);
    if (!isCorrectPass) {
      throw new UnauthorizedException('Parol ne pravilnii');
    }

    return { email: user.email };
  }

  async login(email: string) {
    const payLoad = { email };
    return {
      access_token: await this.jwtService.signAsync(payLoad),
    };
  }
}
