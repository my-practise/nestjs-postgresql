import {
  Controller,
  HttpCode,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { JWTAuthGuard } from '../auth/guards/auth.guard';
import { FileResponseElementResponse } from './dto/file-response-element.response';
import { FileService } from './file.service';
import { Mfile } from './mfile';

@Controller('file')
export class FileController {
  constructor(private readonly fileService: FileService) {}
  @Post('upload')
  @HttpCode(200)
  //@UseGuards(JWTAuthGuard)
  @UseInterceptors(FileInterceptor('files'))
  async uploadFile(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<FileResponseElementResponse[]> {
    const arr: Mfile[] = [new Mfile(file)];
    if (file.mimetype.includes('image')) {
      const buffer = await this.fileService.convertWebP(file.buffer);
      arr.push(
        new Mfile({
          originalname: `${file.originalname.split('.')[0]}.webp`,
          buffer,
        }),
      );
    }
    return this.fileService.saveFiles(arr);
  }
}
