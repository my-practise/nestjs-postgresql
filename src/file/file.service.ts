import { Injectable } from '@nestjs/common';
import { FileResponseElementResponse } from './dto/file-response-element.response';
import { format } from 'date-fns';
import { path } from 'app-root-path';
import { ensureDir, writeFile } from 'fs-extra';
import * as sharp from 'sharp';
import { Mfile } from './mfile';

@Injectable()
export class FileService {
  async saveFiles(files: Mfile[]): Promise<FileResponseElementResponse[]> {
    const dateFile = format(new Date(), 'yyyy-MM-dd');
    const uploadFolder = `${path}/uploads/${dateFile}`;
    await ensureDir(uploadFolder);
    const res: FileResponseElementResponse[] = [];
    for (const f of files) {
      await writeFile(`${uploadFolder}/${f.originalname}`, f.buffer);
      res.push({ url: `${dateFile}/${f.originalname}`, name: f.originalname });
    }
    return res;
  }

  async convertWebP(file: Buffer): Promise<Buffer> {
    return sharp(file).webp().toBuffer();
  }
}
