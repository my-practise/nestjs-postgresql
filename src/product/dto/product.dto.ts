import {
  IsArray,
  IsNumber,
  IsOptional,
  ValidateNested,
  IsString,
  IsJSON,
} from 'class-validator';
import { Type } from 'class-transformer';
// class ProductCharacteristicDto {
//   @IsString()
//   name: string;
//   @IsString()
//   value: string;
// }
export class ProductDto {
  @IsString()
  image: string;
  @IsString()
  title: string;
  @IsNumber()
  price: number;

  @IsOptional()
  @IsNumber()
  oldPrice?: number;

  @IsNumber()
  credit: number;
  @IsString()
  description: string;
  @IsString()
  advantages: string;
  @IsString()
  disAdvantages: string;

  @IsArray()
  @IsString({ each: true })
  categories: string[];

  @IsArray()
  @IsString({ each: true })
  tags: string[];

  @IsString({ each: true })
  characteristics: {
    [key: string]: string;
  };
}
