import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { ReviewEntity } from '../review/review.entity';

@Entity('products')
export class ProductEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  image: string;
  @Column()
  title: string;
  @Column()
  price: number;
  @Column()
  oldPrice?: number;
  @Column()
  credit: number;
  @Column()
  description: string;
  @Column()
  advantages: string;
  @Column()
  disAdvantages: string;
  @Column({ type: 'simple-array' })
  categories: string[];
  @Column({ type: 'simple-array' })
  tags: string[];
  @Column('jsonb')
  characteristics: {
    name: string;
    value: string;
  };

  @OneToMany(() => ReviewEntity, (review) => review.products)
  review: ReviewEntity[];
}
