import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  NotFoundException,
  Param,
  Patch,
  Post, UseGuards, UsePipes, ValidationPipe
} from "@nestjs/common";
import { ProductDto } from './dto/product.dto';
import { FindProductDto } from './dto/findproduct.dto';
import { ProductService } from './product.service';
import { JWTAuthGuard } from "../auth/guards/auth.guard";

@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}
  @UseGuards(JWTAuthGuard)
  @UsePipes(new ValidationPipe())
  @Post('create')
  async create(@Body() dto: ProductDto) {
    return this.productService.create(dto);
  }
  @UseGuards(JWTAuthGuard)
  @Get(':id')
  async get(@Param('id') id: number) {
    const product = await this.productService.findById(id);

    if (!product) {
      throw new NotFoundException('нет такой продукт');
    }
    return product;
  }
  @UseGuards(JWTAuthGuard)
  @Delete(':id')
  async delete(@Param('id') id: number) {
    const product = await this.productService.delete(id);

    if (!product) {
      throw new NotFoundException('нет такой продукт');
    }
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() dto: ProductDto) {
    const updateproduct = await this.productService.update(dto, id);

    if (!updateproduct) {
      throw new NotFoundException('нет такой продукт');
    }
    return updateproduct;
  }

  @HttpCode(200)
  @UsePipes(new ValidationPipe())
  @Post('find')
  async find(@Body() dto: FindProductDto) {
    return this.productService.findWithReview(dto);
  }
}
