import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthEntity } from '../auth/auth.entity';
import { Repository } from 'typeorm';
import { ProductEntity } from './product.entity';
import { ProductDto } from './dto/product.dto';
import { FindProductDto } from './dto/findproduct.dto';
import { ReviewEntity } from '../review/review.entity';
import { loadavg } from 'os';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(ProductEntity)
    private readonly productEntity: Repository<ProductEntity>,
  ) {}

  async create(dto: ProductDto) {
    return this.productEntity.save(dto);
  }

  async findById(id: number): Promise<ProductEntity> {
    return await this.productEntity.findOne({
      relations: ['review'],
      where: { id: id },
    });
  }

  async delete(id: number) {
    return this.productEntity.delete(id);
  }

  async update(dto: ProductDto, id: number) {
    return this.productEntity.update({ id: id }, { ...dto });
  }

  async findWithReview(dto: FindProductDto): Promise<ProductEntity[]> {
    const productAndReview = await this.productEntity.find({
      relations: {
        review: true,
      },
      select: {

        review: {
          rating: true,
        },
      },
      take: dto.limit,
      where: {
        categories: dto.category,
      },

      order: {
        id: 'ASC',
      },
    });

    // const productAvg = await this.productEntity
    //   .createQueryBuilder('product')
    //
    //
    //   .leftJoinAndSelect('product.review', 'review')
    //
    //   .where(' categories= :category', { category: dto.category })
    //   .getMany();

    return productAndReview;
  }
}
